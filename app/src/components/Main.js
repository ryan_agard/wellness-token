import React, { useState, useEffect } from "react";
import levelUpLogo from "../levelupLogo.png";
import * as Promise from "bluebird";

export default ({ drizzle, drizzleState }) => {

  const contract = drizzle.contracts.WellnessToken;
  const accounts = drizzleState.accounts;
  console.log('drizzleState: ', drizzleState);
  console.log('drizzle: ', drizzle);

  const [wellnessProviders, setWellnessProviders] = useState([]);
  const [totalSupply, setTotalSupply] = useState(0);
  const [allowanceState, setAllowance] = useState(0);
  const [balance, setBalance] = useState(null);
  const [receipts, setReciepts] = useState([]);

  useEffect(() => {
    getTotalSupply();
    getWellnessProviders();
    getTokenOwner();
    // getWellnessAddresses();
  }, []);

  const wellnessProvidersAddresses = async (providers) => {
    return new Promise((resolve, reject) => {
      Promise.map(providers, (provider) => {
        return new Promise((resolveMap, rejectMap) => {
          return contract.methods.getAddressByProvider(provider).call().then((address) => {
            console.log('address: ', address);
            return resolveMap({ address, provider });
          }).catch((err) => {
            return rejectMap(err);
          })
        })
      }, {
        concurrency: 1,
      }).then((providerData) => {
        return resolve(providerData);
      }).catch((err) => {
        return reject(err);
      })
    })
  }

  const getWellnessProviders = async () => {
    const wellnessProvidersRaw = await contract.methods.getWellnessProviders().call();
    const wellnessProviders = wellnessProvidersRaw.filter((provider) => provider !== "");
    const wellnessProviderData = await wellnessProvidersAddresses(wellnessProviders);
    setWellnessProviders(wellnessProviderData);
  }

  const getTokenOwner = async () => {
    const owner = await contract.methods.tokenOwner().call();
    console.log('owner: ', owner);
  }

  const getAllowance = async (index) => {
    const allowance = await contract.methods.allowance(accounts[0], accounts[index]).call();
    setAllowance(allowance);
    console.log('allowance: ', allowance);
  }

  const approveDelegate = async (index) => {
    const approveResponse = await contract.methods.approve(accounts[index], 500).send();
    console.log('approveResponse: ', approveResponse);
  }

  const distributeTokens = async (index, numOfTokens) => {
    const response = await contract.methods.transfer(accounts[index], numOfTokens).send();
    console.log('response: ', response);
  }

  const getTotalSupply = async () => {
    const totalSupply = await contract.methods.totalSupply().call();
    console.log('totalSupply: ', totalSupply);
    setTotalSupply(totalSupply);
  }

  const getBalanceOf = async (index) => {
    const balance = await contract.methods.balanceOf(accounts[index]).call();
    console.log('balance: ', balance);
    setBalance({ balance, index });
  }

  // const getWellnessAddresses = async () => {
  //   const res = await contract.methods.getAddressByWellnessName("Wellness Name").call();
  //   console.log('res: ', res);
  // }

  const addWellnessProvider = async (name, index) => {
    const receipt = await contract.methods.addWellnessProvider(name, accounts[index]).send();
    console.log('receipt: ', receipt);
    getWellnessProviders();
  }

  const payWellnessProvider = async (provider, tokens, userAddress) => {
    const wellnessAddress = wellnessProviders.filter((wellnessObj) => wellnessObj.provider.toLowerCase() === provider.toLowerCase());
    if(wellnessAddress.length) {
      // we must approve userAddress to send tokens to wellnessAddress
      const approveResponse = await contract.methods.approve(wellnessAddress[0].address, tokens).send({ from: userAddress });
      console.log('approveResponse: ', approveResponse);

      // // checking allowance of tokens to be transferred from userAddress to ticketAddress
      // const allowance = await contract.methods.allowance(userAddress, ticketAddress[0].address).call();
      // console.log('allowance: ', allowance);

      // transferring tokens from userAddress to ticketAddress
      const receipt = await contract.methods.transferFrom(userAddress, wellnessAddress[0].address, tokens).send({ from: wellnessAddress[0].address });
      const receiptsCopy = receipts;
      receiptsCopy.push(receipt);
      setReciepts(receiptsCopy);
      console.log('receipt: ', receipt);
    } else {
      console.log('invalid provider name');
    }
  }

  const getProviderIndex = (address) => {
    for (let key in accounts) {
      if (accounts[key] === address) {
        return key;
      }
    }
    return '';
  }

  return (
    <div className={"main"}>
      <div className={"logoContainer"}>
        <img src={levelUpLogo} alt="level-up-logo" />
      </div>
      <h1>Wellness Token</h1>
      <button onClick={() => {
        console.log('receipts: ', receipts);
        console.log('wellnessProviders: ', wellnessProviders);
      }}>log</button>

      <div className={"sectionContainer"}>
        <h3>Wellness Providers</h3>
        {wellnessProviders.length ? (
          <div style={{ width: '100%' }}>
            {wellnessProviders.map((data, i) => {
              if (data.address && data.provider) {
                return (
                  <div key={i} style={{ width: '77%', display: 'flex', justifyContent: 'space-between', paddingLeft: '40px' }}>
                    <p>{`Index: ${getProviderIndex(data.address)}`}</p>
                    <p>{`Provider: ${data.provider}`}</p>
                  </div>
                )
              }
            })}
          </div>
        ) : null}
      </div>

      <div className={"sectionContainer"}>
        <h2>Pay Wellness Provider</h2>
        <div className={"logHoursInputContainer"}>
          <div className={"logHoursInputWrapper"}>
            <p>Wellness Provider Name</p>
            <input type="text" id="ticketNumber" className={"inputField"}></input>
          </div>
          <div className={"logHoursInputWrapper"}>
            <p>Tokens</p>
            <input type="text" id="numOfHours" className={"inputField"}></input>
          </div>
          <div className={"logHoursInputWrapper"}>
            <p>User ID</p>
            <input type="text" id="userId" className={"inputField"}></input>
          </div>
          <div style={{ width: '375px', marginLeft: '-40px' }}>
            <p>{"For now, Address ID field must be a number between 1 and 5.  These numbers respresent an account index for an ethereum address"}</p>
          </div>
          <div className={"logHoursBtnContainer"}>
            <button
              style={{ width: '75px' }}
              onClick={() => {
                const inp1 = document.getElementById('ticketNumber').value;
                const inp2 = parseInt(document.getElementById('numOfHours').value);
                const inp3 = parseInt(document.getElementById('userId').value);
                payWellnessProvider(inp1, inp2, accounts[inp3])
              }}>Submit</button>
          </div>
        </div>
      </div>

      <div className={"sectionContainer"}>
        <h3>Add New Wellness Provider</h3>
        <div style={{ padding: '0px 30px 0px 30px' }}>
          <p>{"Enter an address index between 6 and 9.  You can't use an index that's already displayed in the 'Wellness Provider' section above"}</p>
        </div>
        <div className={"logHoursInputWrapper"}>
          <p>Wellness Provider Name</p>
          <input type="text" id="addTicketNumber" className={"inputField"}></input>
        </div>

        <div className={"logHoursInputWrapper"}>
          <p>Address ID</p>
          <input type="text" id="addTicketAddress" className={"inputField"}></input>
        </div>

        <div className={"logHoursBtnContainer"}>
          <button
            style={{ width: '75px' }}
            onClick={() => {
              const wellnessName = document.getElementById('addTicketNumber').value;
              const accountIndex = parseInt(document.getElementById('addTicketAddress').value);
              addWellnessProvider(wellnessName, accountIndex);
            }}>Submit</button>
        </div>
      </div>

      {/* <div className={"sectionContainer"}>
        <h3>Approve account for transfer</h3>
        <div style={{ padding: '0px 100px 0px 110px' }}>
          <p>{"Enter account index to be approved for 1000 tokens"}</p>
        </div>
        <div className={"allowanceBtnContainer"}>
          <input type="text" id="approve" style={{ width: '97px' }}></input>
          <button
            style={{ width: '105px' }}
            onClick={() => {
              const delegateIndex = parseInt(document.getElementById('approve').value);
              approveDelegate(delegateIndex);
            }}>approve</button>
        </div>
      </div> */}

      <div className={"sectionContainer"}>
        <h3>Distribute tokens to account</h3>
        <div className={"logHoursInputContainer"}>
          <div className={"logHoursInputWrapper"}>
            <p>Account ID</p>
            <input type="text" id="distributeAddress" className={"inputField"}></input>
          </div>
          <div className={"logHoursInputWrapper"}>
            <p>Number of tokens to transfer</p>
            <input type="text" id="numOfTokens" className={"inputField"}></input>
          </div>
          <div className={"logHoursBtnContainer"}>
            <button style={{ marginTop: '10px' }} onClick={() => {
              const addr = document.getElementById('distributeAddress').value;
              const numOfTokens = parseInt(document.getElementById('numOfTokens').value);
              distributeTokens(addr, numOfTokens);
            }}>{'Distribute'}</button>
          </div>
        </div>
      </div>

      <div className={"sectionContainer"}>
        <h3>Check balance of account by index</h3>
        <div className={"allowanceBtnContainer"}>
          <input type="text" id="accountIndex" style={{ width: '97px' }}></input>
          <button
            style={{ width: '105px' }}
            onClick={() => {
              const accountIndex = parseInt(document.getElementById('accountIndex').value);
              getBalanceOf(accountIndex);
            }}>balanceOf</button>
        </div>
        {balance ? (
            <div style={{ marginTop: '10px' }}>
              <p>{`Balance of Account # ${balance.index}: `}<span style={{ fontWeight: "bold" }}>{balance.balance}</span></p>
            </div>
          ) : null}
      </div>

      {/* <div className={"sectionContainer"}>
        <h3>Check Allowance</h3>
        <div className={"checkAllowanceBody"}>
          <div style={{ padding: "10px" }}>
            <p>{"Enter an index 1 - 5 to check that account's allowance. Allowance refers to the number of tokens authorized to be transfered to that account"}</p>
            <p>{"If the displayed value is 0, the given account must be approved before tokens are transferred"}</p>
          </div>

        </div>
        <div className={"allowanceBtnContainer"}>
          <input type="text" id="allowance" style={{ width: '97px' }}></input>
          <button
            style={{ width: '105px' }}
            onClick={() => {
              const allowanceVal = parseInt(document.getElementById('allowance').value);
              getAllowance(allowanceVal);
            }}>get allowance</button>

        </div>
        {allowanceState ? (
          <div>
            <p>{"Allowance: "}<span style={{ fontWeight: "bold" }}>{allowanceState}</span></p>
          </div>
        ) : null}
      </div> */}

      <div className={"sectionContainer"} style={{ marginBottom: '50px' }}>
        <h3>Total Supply</h3>
        <div>
          <p>{"Total Supply of Wellness Tokens: "}<span style={{ fontWeight: "bold", paddingLeft: '3px' }}>{totalSupply}</span></p>
        </div>
      </div>
    </div>
  );
};
