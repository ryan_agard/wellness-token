import Web3 from "web3";
import WellnessToken from "./contracts/WellnessToken.json";

const options = {
  web3: {
    block: false,
    customProvider: new Web3("ws://localhost:8545"),
  },
  contracts: [WellnessToken],
  events: {
    SimpleStorage: ["StorageSet"],
  },
};

export default options;
