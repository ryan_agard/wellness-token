// SPDX-License-Identifier: MIT
pragma solidity >=0.4.21 <0.7.0;
pragma experimental ABIEncoderV2;
import "@openzeppelin/contracts/token/ERC20/ERC20.sol";

contract WellnessToken is ERC20 {
    string public name = "WellnessToken";
    string public symbol = "WT";
    uint256 public decimals = 2;
    uint256 public INITIAL_SUPPLY = 12000;
    uint256 totalSupply_;
    address public tokenOwner;

    mapping(string => address) wellness_addresses;
    mapping(address => uint256) balances;
    mapping(address => mapping (address => uint256)) allowed;

    string[] public wellnessProviders = new string[](1000);

    constructor() public {
        _mint(msg.sender, INITIAL_SUPPLY);
        tokenOwner = msg.sender;
        totalSupply_ = INITIAL_SUPPLY;
        approve(msg.sender, totalSupply_);
    }

    function addWellnessProvider(string memory wellnessName, address _address) public returns (bool) {
        wellness_addresses[wellnessName] = _address;
        wellnessProviders.push(wellnessName);
        return true;
    }

    function getWellnessProviders() external view returns (string[] memory) {
        return wellnessProviders;
    }

    function getAddressByProvider(string memory providerName) public view returns (address) {
        return wellness_addresses[providerName];
    }
}
